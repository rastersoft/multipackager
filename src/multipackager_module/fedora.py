#!/usr/bin/env python3

# Copyright 2015 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of Multipackager
#
# Multipackager is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Multipackager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import os
import sys
import shutil
import configparser
import multipackager_module.package_base
import requests
import json

class fedora (multipackager_module.package_base.package_base):

    def __init__(self, configuration, distro_type, distro_name, architecture, cache_name = None):

        if architecture == "amd64":
            architecture = "x86_64"

        super().__init__(configuration, distro_type, distro_name, architecture,cache_name)
        self.distro_number = int(self.distro_name)


    def check_path_in_builds(self,project_path):

        path_list = ["rpmbuild/SPECS","RPM/SPECS","rpm/SPECS","rpmbuild","RPM","rpm"]

        for element in path_list:
            path = os.path.join(project_path,element)
            if os.path.exists(path):
                return path
        return None


    def get_package_name(self, project_path):
        """ Returns the final package name for the project specified, or None if can't be determined yet """

        self.read_specs_data(project_path)

        if multipackager_module.package_base.package_base.check_is_python(project_path)[0]:
            return [f"{self.project_name}.{self.distro_type}{self.distro_name}-{self.project_version}-{self.configuration.revision}.noarch.rpm"]
        else:
            return [f"{self.project_name}.{self.distro_type}{self.distro_name}-{self.project_version}-{self.configuration.revision}.{self.architecture}.rpm"]

    def _store_config_data(self, tmp_path, version):
        yumcfgpath = os.path.join(tmp_path,"yum.conf")
        yumrepospath = os.path.join(tmp_path,"yum.repos.d")

        yumcfg = open(yumcfgpath,"w")
        yumcfg.write("[main]\n")
        yumcfg.write("cachedir=/var/cache/yum\n")
        yumcfg.write("persistdir=/var/lib/yum\n")
        yumcfg.write("keepcache=0\n")
        yumcfg.write("debuglevel=2\n")
        yumcfg.write(f"logfile={os.path.join(tmp_path,'build.log')}\n")
        yumcfg.write("exactarch=0\n")
        yumcfg.write("obsoletes=1\n")
        yumcfg.write("gpgcheck=1\n")
        yumcfg.write("plugins=1\n")
        yumcfg.write("installonly_limit=3\n")
        yumcfg.write(f"reposdir={yumrepospath}\n")
        yumcfg.close()

        yumrepos = open(os.path.join(yumrepospath,"fedora.repo"),"w")
        yumrepos.write("[fedora]\n")
        yumrepos.write(f"name=Fedora {version} - {self.architecture}\n") # the last that works; we will change it after
        yumrepos.write("failovermethod=priority\n")
        yumrepos.write(f"metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-{version}&arch={self.architecture}\n")
        yumrepos.write("enabled=1\n")
        yumrepos.write("medatada_expire=7d\n")
        yumrepos.write("gpgcheck=1\n")
        yumrepos.write(f"gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-{version}-{self.architecture}\n")
        yumrepos.write("skip_if_unavailable=False\n")
        yumrepos.close()

    def _delete_tmp(self, path, all):
        for name in [".dload", ".tmp", ".layer"]:
            shutil.rmtree(path + name, ignore_errors=True)
        if all:
            shutil.rmtree(path, ignore_errors=True)

    def generate(self, path):
        """ Ensures that the base system, to create a CHROOT environment, exists """

        self.create_tmp_path(path)
        tmp_path = self.create_tmp_path(path,".tmp")
        dload_path = self.create_tmp_path(path,".dload")
        layer_path = self.create_tmp_path(path,".layer")

        url = f"https://download.fedoraproject.org/pub/fedora/linux/releases/{self.distro_number}/Container/{self.architecture}/images/"
        data = requests.get(url).text
        pos = data.find(f'<a href="Fedora-Container-Base-Generic-{self.distro_number}')
        if (pos == -1):
            self._delete_tmp(path, True)
            print(f"Can't download container image from {url} 1")
            return True #error!
        pos2 = data.find('"', pos + 10)
        if (pos2 == -1):
            self._delete_tmp(path, True)
            print(f"Can't download container image from {url} 2")
            return True #error!
        filename = data[pos + 9:pos2]

        if 0 != self.run_external_program(f"wget {url}{filename} -O {dload_path}/{filename}"):
            self._delete_tmp(path, True)
            print(f"Can't download container image {url}{filename}")
            return True # error!!!

        if 0 != self.run_external_program(f"tar xf {dload_path}/{filename} -C {layer_path}"):
            self._delete_tmp(path, True)
            print(f"Can't extract the container {filename}")
            return True # error!!!

        with open(os.path.join(layer_path, "index.json"), "r") as index_f:
            index = json.load(index_f)

        with open(os.path.join(layer_path, "blobs", index["manifests"][0]["digest"].replace(":", os.path.sep)), "r") as manifest_f:
            manifest = json.load(manifest_f)

        layer = os.path.join(layer_path, "blobs", manifest['layers'][0]["digest"].replace(":", os.path.sep))
        if 0 != self.run_external_program(f"tar xf {layer} -C {tmp_path}"):
            self._delete_tmp(path, True)
            print(f"Can't extract the container {filename}")
            return True # error!!!

        packages = "fedora-release bash dnf util-linux meson"
        # for some reason, the RPM database is not complete, so it is a must to reinstall everything from inside the chroot environment
        command = f'dnf -y --nogpgcheck --releasever={self.distro_name} install {packages}'
        if (0 != self.run_chroot(tmp_path, command)):
            self._delete_tmp(path, True)
            return True # error!!!

        command = f'dnf -y  --nogpgcheck --releasever={self.distro_name} update'
        if 0 != self.run_chroot(tmp_path, command):
            self._delete_tmp(path, True)
            return True # error!!!

        os.sync()
        os.rename(tmp_path,path) # rename the folder to the definitive name
        os.sync()

        self._delete_tmp(path, False)

        return False # no error


    def update(self,path):

        """ Ensures that the chroot environment is updated with the lastest packages """

        # Here, we have for sure the CHROOT environment, but maybe it must be updated
        command = f'dnf  --releasever={self.distro_name} update -y'
        if (0 != self.run_chroot(path,command)):
            return True # error!!!

        return False


    def read_specs_data(self,working_path):

        if working_path == None:
            return

        self.dependencies = []
        self.dependencies.append("rpm-build")

        is_python, pyfile = multipackager_module.package_base.package_base.check_is_python(working_path)
        if is_python: # it is a python package
            self.read_python_setup(working_path, pyfile)
            self.dependencies.append("python3")
            setup_cfg = os.path.join(working_path,"setup.cfg")
            if os.path.exists(setup_cfg):
                pkg_data = configparser.ConfigParser()
                pkg_data.read(setup_cfg)
                if 'bdist_rpm' in pkg_data:
                    if 'build_requires' in pkg_data['bdist_rpm']:
                        deps = pkg_data['bdist_rpm']['build_requires'].split(' ')
                        for dep in deps:
                            dep = dep.strip()
                            if dep == '':
                                continue
                            self.dependencies.append(dep)
                    if 'name' in pkg_data['bdist_rpm']:
                        self.project_name = pkg_data['bdist_rpm']['name']
                    if 'version' in pkg_data['bdist_rpm']:
                        self.project_version = pkg_data['bdist_rpm']['version']
        else:
            self.dependencies.append("meson")
            self.dependencies.append("ninja-build")
            self.dependencies.append("rpmlint")
            specs_path = self.check_path_in_builds(working_path)
            if (specs_path == None):
                print("No rpm folder. Aborting.")
                return True

            if not (os.path.exists(specs_path)):
                print("The project lacks the rpmbuild/SPECS folder. Aborting.")
                return True
            files = os.listdir(specs_path)
            self.final_file = None
            for l in files:
                if len(l) < 5:
                    continue
                if l[-5:] != ".spec":
                    continue
                self.final_file = os.path.join(specs_path,l)
                break;

            if (self.final_file == None):
                print("No .spec file found. Aborting.")
                return True

            spec = open(self.final_file,"r")
            for line in spec:
                if line[:14] == "BuildRequires:":
                    self.dependencies.append(line[14:].strip())
                    continue
                if line[:9] == "Requires:":
                    #self.dependencies.append(line[9:].strip())
                    continue
                if line[:5] == "Name:":
                    self.project_name = line[5:].strip()
                    continue
                if line[:8] == "Version:":
                    self.project_version = line[8:].strip()
                    continue
                if line[:8] == "Release:":
                    continue


    def install_dependencies_full(self,path,deps):

        command = f"dnf --releasever={self.distro_name} -y install"
        for dep in deps:
            command += " "+dep
        return self.run_chroot(path, command)


    def install_local_package_internal(self, file_name):

        command = f"dnf --releasever={self.distro_name} install -y {file_name}"

        if 0 != self.run_chroot(self.working_path, command):
            return True
        return False


    def install_dependencies(self,project_path,avoid_packages,preinstall):

        """ Install the dependencies needed for building this package """

        if self.read_specs_data(project_path):
            return True

        deps = []
        for d in self.dependencies:
            if avoid_packages.count(d) == 0:
                deps.append(d)

        if (len(deps) != 0):
            return self.install_dependencies_full(self.base_path,deps)
        return False


    def build_python(self):
        """ Builds a package for a python project """

        destination_dir = os.path.join(self.build_path,"dist")
        shutil.rmtree(destination_dir, ignore_errors = True)

        if (self.run_chroot(self.working_path, 'bash -c "cd /project && python3 setup.py bdist_rpm"')):
            return True

        return False


    def copy_rpms(self,destination_dir):

        files = os.listdir(destination_dir)
        for f in files:
            if f[-11:] == ".noarch.rpm":
                origin_name = os.path.join(destination_dir,f)
                if os.path.isdir(origin_name):
                    if not self.copy_rpms(origin_name):
                        return False
                    continue
                final_name = os.path.join(os.getcwd(),self.get_package_name(self.build_path)[0])
                if (os.path.exists(final_name)):
                    os.remove(final_name)
                shutil.move(origin_name, final_name)
                return False
        return True


    def copy_bin_rpms(self,destination_dir):

        files = os.listdir(destination_dir)
        for f in files:
            origin_name = os.path.join(destination_dir,f)
            if os.path.isdir(origin_name):
                if not self.copy_bin_rpms(origin_name):
                    return False
                continue
            final_name = os.path.join(os.getcwd(),self.get_package_name(self.build_path)[0])
            if (os.path.exists(final_name)):
                os.remove(final_name)
            if f[-4:] == ".rpm":
                shutil.move(origin_name, final_name)
                return False
        return True


    def build_package(self,project_path):
        """ Takes the binaries located at /install_root and build a package """

        shutil.rmtree(os.path.join(self.working_path, "rpmpackage"), ignore_errors=True)

        if multipackager_module.package_base.package_base.check_is_python(self.build_path)[0]:
            destination_dir = os.path.join(self.build_path,"dist")
            return self.copy_rpms(destination_dir)

        tmpfolder = self.check_path_in_builds(self.build_path)
        if (tmpfolder == None):
            print ("Can't find the rpmbuild folder. Aborting.")
            return True

        for d in ["SPECS", "SOURCES", "BUILD", "RPMS", "SRPMS"]:
            os.makedirs(os.path.join(self.working_path,"rpmpackage", d))

        try:
            f = open(os.path.join(self.working_path,"root/.rpmmacros"),"w")
            f.write("%_topdir /rpmpackage\n")
            f.write("%_builddir %{_topdir}/BUILD\n")
            f.write("%_rpmdir %{_topdir}/RPMS\n")
            f.write("%_sourcedir %{_topdir}/SOURCES\n")
            f.write("%_specdir %{_topdir}/SPECS\n")
            f.write("%_srcrpmdir %{_topdir}/SRPMS\n")
            f.close()
        except:
            print ("Can't create the .rpmmacros file. Aborting.")
            return True

        spec_i = open(self.final_file,"r")
        spec_o = open(os.path.join(self.working_path,"rpmpackage/SPECS",self.project_name+".specs"),"w")
        do_copy = True
        for line in spec_i:
            line = line.strip()
            if (line == ""):
                do_copy = True
                spec_o.write("\n")
                continue

            if do_copy == False:
                continue

            if line[:8] == "Release:":
                spec_o.write(f"Release: {self.configuration.revision}\n")
                continue

            if line[0] != '%':
                spec_o.write(line+"\n")
                continue

            if line[:5] == "%prep":
                spec_o.write("%prep\n")
                do_copy = False
                continue
            if line[:6] == "%build":
                spec_o.write("%build\n")
                do_copy = False
                continue
            if line[:8] == "%install":
                spec_o.write("%install\n")
                spec_o.write("rm -rf $RPM_BUILD_ROOT/*\n")
                spec_o.write("cp -a /install_root/* $RPM_BUILD_ROOT/\n")
                do_copy = False
                continue
            if line[:6] == "%clean":
                spec_o.write("%clean\n")
                spec_o.write("rm -rf $RPM_BUILD_ROOT/*\n")
                do_copy = False
                continue
            if line[:6] == "%files":
                spec_o.write("%files\n")
                spec_o.write("/*\n")
                do_copy = False
                continue

            spec_o.write(line+"\n")
        spec_i.close()
        spec_o.close()

        command = f"rpmbuild -bb {os.path.join('/rpmpackage/SPECS',self.project_name+'.specs')}"
        if (self.run_chroot(self.working_path, command)):
            return True

        return self.copy_bin_rpms(os.path.join(self.working_path,"rpmpackage","RPMS"))
