#!/usr/bin/env python3

# Copyright 2015 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of Multipackager
#
# Multipackager is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Multipackager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import subprocess
import os
import shutil
import re
import stat
import toml
import logging

class package_base(object):

    @staticmethod
    def check_is_python(project_path):
        if (os.path.exists(os.path.join(project_path,"pyproject.toml"))):
            return True, "pyproject.toml"

        if (os.path.exists(os.path.join(project_path,"setup.py"))):
            return True, "setup.py"
        return False, None

    def create_tmp_path(self, path, name = ""):
        tmp_path = path+name
        shutil.rmtree(tmp_path, ignore_errors = True)
        os.makedirs(tmp_path)
        return tmp_path


    def full_delete(self,path):
        if os.path.exists(path) is False:
            return

        status = os.lstat(path)
        if stat.S_ISDIR(status.st_mode) is True:
            shutil.rmtree(path)
        else:
            os.remove(path)


    def __init__(self, configuration, distro_type, distro_name, architecture, cache_name = None):
        self.install_at_lib = False
        self.configuration = configuration
        self.distro_type = distro_type
        self.distro_name = distro_name
        self.architecture = architecture
        self.project_name = "project"
        self.project_version = "1.0"
        self.project_release = "1"
        self.python2 = False
        self.distro_full_name = f"{distro_type} {distro_name} {architecture}"
        self.program_size = 0
        self.env = {}
        self.log = logging.getLogger(__name__)
        self.log.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(formatter)
        self.log.addHandler(console_handler)
        #logging.basicConfig(filename='myapp.log', level=logging.INFO)

        # name of the CHROOT environment to use
        self.base_chroot_name = self.distro_type+"_chroot_"+self.distro_name+"_"+self.architecture

        if (cache_name != None):
            self.chroot_name = self.base_chroot_name+"_"+cache_name
        else:
            self.chroot_name = self.base_chroot_name

        # path of the base CHROOT enviromnent to use (the one which will be copied to the final one)
        self.base_path = os.path.join(self.configuration.cache_path,self.chroot_name)

        if (self.base_path[-1] == os.path.sep):
            self.base_path = self.base_path[:-1] # remove the last "/" if it exists

        # path of the working copy, where the project has been copied for being build
        self.working_path = None

        # path of the project copy (outside the chroot environment; inside is always "/project")
        self.build_path = None

        # this is the data stored in the setup.py script (if it is a python program)
        self.pysetup = {}


    def install_local_package(self, file_path):
        shutil.copy(file_path,self.working_path)
        if self.install_local_package_internal(os.path.join(os.path.sep,os.path.basename(file_path))):
            return True
        self.log.info(f"{file_path} correctly installed")
        return False


    def add_dns(self, path):
        # Add OpenDNS
        resolvconf_path = os.path.join(path, "etc", "resolv.conf")
        self.log.debug(f"Adding DNS file at {resolvconf_path}")
        try:
            os.unlink(resolvconf_path)
        except:
            pass
        try:
            with open(resolvconf_path,"w") as f:
                f.write("# OpenDNS IPv4 nameservers\nnameserver 208.67.222.222\nnameserver 208.67.220.220\n") # OpenDNS IPv6 nameservers\nnameserver 2620:0:ccc::2\nnameserver 2620:0:ccd::2\n")
        except:
            pass


    def check_environment(self):
        """ Ensures that the base_path with an updated system exists """

        if not os.path.exists(self.base_path):
            self.log.info(f"Generating the environment for {self.base_chroot_name}")
            if self.generate(self.base_path):
                self.log.warning(f"Failed to initializate environment for {self.base_chroot_name}")
                return True # error!!!
        self.add_dns(self.base_path)
        return False


    def install_postdependencies(self, project_path):
        return False


    def prepare_working_path(self, final_path = None):
        if final_path == None:
            self.working_path = os.path.join(self.configuration.working_path, self.base_chroot_name)
        else:
            self.working_path = final_path
        self.log.info(f"Working path set to {self.working_path}")
        # environment ready
        return False


    def update_environment(self):
        """ Ensures that the environment is updated with the last packages """

        self.log.info(f"Updating {self.base_chroot_name}")
        return self.update(self.base_path)


    def clear_cache(self):
        if os.path.exists(self.base_path):
            self.log.info(f"Cleaning cache at {self.base_path}")
            shutil.rmtree(self.base_path, ignore_errors = True)


    def get_project_size(self):
        """ Calculates the size of all the files that will be installed in the final system """

        sum = 0
        final_path = os.path.join(self.working_path,"install_root")
        for dirname, dirnames, filenames in os.walk(final_path):
            for filename in filenames:
                sum += os.path.getsize(os.path.join(dirname,filename))

        self.program_size = sum
        self.log.info(f"Project size: {sum}")


    def build_project(self,project_path):
        """ Builds the specified project inside the working copy of the chroot environment """

        self.build_path = os.path.join(self.working_path, "project")

        shutil.rmtree(self.build_path, ignore_errors=True)
        os.makedirs(self.build_path)

        # copy the project folder inside the CHROOT environment, in the "/project" folder

        if 0 != self.run_external_program(f"cp -a {os.path.join(project_path,"*")} {os.path.join(self.working_path,"project/")}"):
            return True # error!!!

        # the compiled binaries will be installed in /install_root, inside the chroot environment
        install_path = os.path.join(self.working_path,"install_root")
        shutil.rmtree(install_path, ignore_errors = True)
        os.makedirs (install_path)

        specific_creator = f"multipackager_{self.distro_type}.sh"
        # check the install system available
            # First, check if exists a shell file called 'multipackager_DISTROTYPE.sh' (like multipackager_debian.sh, or multipackager_ubuntu.sh)
        if (os.path.exists(os.path.join(self.build_path,specific_creator))):
            if self.build_multipackager(specific_creator):
                return True
            # Now check if it exists a generic shell script called 'multipackager.sh'
        elif (os.path.exists(os.path.join(self.build_path,"multipackager.sh"))):
            if self.build_multipackager("multipackager.sh"):
                return True
            # Check if it is a python program
        elif (package_base.check_is_python(self.build_path)[0]):
            if self.build_python():
                return True
            # Check if it is an autoconf/automake with the 'configure' file already generated
        elif (os.path.exists(os.path.join(self.build_path,"configure"))):
            if self.build_autoconf(False):
                return True
            # Now check if it is an autoconf/automake without the 'configure' file generated
        elif (os.path.exists(os.path.join(self.build_path,"autogen.sh"))):
            if self.build_autoconf(True):
                return True
            # Check if it is a Meson project
        elif (os.path.exists(os.path.join(self.build_path,"meson.build"))):
            if self.build_meson():
                return True
            # Check if it is a CMake project
        elif (os.path.exists(os.path.join(self.build_path,"CMakeLists.txt"))):
            if self.build_cmake():
                return True
            # Finally, try with a classic Makefile
        elif (os.path.exists(os.path.join(self.build_path,"Makefile"))):
            if self.build_makefile():
                return True
        else:
            self.log.critical("Unknown build system")
            return True

        self.copy_perms(self.working_path,install_path)
        return False


    def build_multipackager(self,filename):
        return self.run_chroot(self.working_path, f'cd /project && source {filename} /install_root')


    def build_cmake(self):
        install_path = os.path.join(self.build_path,"install")
        if (os.path.exists(install_path)):
            shutil.rmtree(install_path,ignore_errors = True)
        os.makedirs(install_path)

        if self.install_at_lib:
            return self.run_chroot(self.working_path, 'cd /project/install && cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=/usr/lib && make VERBOSE=1 && make DESTDIR=/install_root install')
        else:
            return self.run_chroot(self.working_path, 'cd /project/install && cmake .. -DCMAKE_INSTALL_PREFIX=/usr && make VERBOSE=1 && make DESTDIR=/install_root install')


    def build_meson(self):
        install_path = os.path.join(self.build_path,"mesonbuild")
        if (os.path.exists(install_path)):
            shutil.rmtree(install_path,ignore_errors = True)
        os.makedirs(install_path)

        return self.run_chroot(self.working_path, 'cd /project/mesonbuild && meson setup --prefix=/usr && ninja && DESTDIR=/install_root ninja install')


    def build_autoconf(self,autogen):
        if (autogen):
            if (self.run_chroot(self.working_path, 'cd /project && ./autogen.sh')):
                return True

        return self.run_chroot(self.working_path, 'cd /project && ./configure --prefix=/usr && make clean && make && make DESTDIR=/install_root install')


    def build_makefile(self):
        return self.run_chroot(self.working_path, 'cd /project && make clean && make && make PREFIX=/usr DESTDIR=/install_root install')


    def get_string(self,line):
        gen_string = re.compile("[\"'][^\"']+[\"']")
        p = gen_string.match(line)
        if p != None:
            return p.group()[1:-1]
        else:
            return None


    def read_python_setup(self, working_path, pyfile):
        final_path = os.path.join(working_path, pyfile)
        if not os.path.exists(final_path):
            return

        for param in [ 'name', 'version', 'author', 'author-email', 'maintainer', 'maintainer-email','url','license','description','long-description','keywords' ]:
            self.pysetup[param] = ""

        if (pyfile == "pyproject.toml"):
            data = toml.load(final_path)
            if "project" in data:
                pdata = data["project"]
                if "name" in pdata:
                    self.project_name = pdata["name"]
                    self.pysetup["name"] = self.project_name
                if "version" in pdata:
                    self.project_version = pdata["version"]
                    self.pysetup["version"] = self.project_version
                if "description" in pdata:
                    self.pysetup["description"] = pdata["description"]
                if "classifiers" in pdata:
                    for entry in pdata["classifiers"]:
                        if entry.startswith("License"):
                            pos = entry.find("::")
                            self.pysetup["license"] = entry[pos+2:].strip()
        elif (pyfile == "setup.py"):
            params = [ 'name', 'version', 'author', 'author-email', 'maintainer', 'maintainer-email','url','license','description','long-description','keywords' ]
            for param in params:
                prc = subprocess.Popen( [final_path , '--'+param], cwd=working_path, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                sout,serr = prc.communicate()
                data = sout.decode().strip()
                self.pysetup[param] = data
                if param == 'name' and data != '':
                    self.project_name = data
                elif param == 'version' and data != '':
                    self.project_version = data


    def run_external_program(self, command, show_msg = True):
        if show_msg:
            self.log.info(f"Launching external command {command}")
        return subprocess.run(command, shell=True).returncode


    def run_chroot(self, base_path, command, in_shell=True, username=None):
        return self.run_chroot_full(base_path, command, capture=False, in_shell=in_shell, username=username).returncode


    def run_chroot_full(self, base_path, command, capture=False, in_shell=True, username=None):
        self.log.info(f"Launching {command}")
        if self.architecture == "i386":
            personality = "x86"
        else:
            personality = "x86-64"

        envs_cmd = ""
        for name in self.env:
            value = self.env[name]
            if -1 == value.find(" "):
                envs_cmd += f" -E {name}={value}"
            else:
                envs_cmd += f' -E {name}="{value}"'
        full_command = f'systemd-nspawn -D {base_path} {envs_cmd} --personality {personality}'
        if username is not None:
            full_command += f' -u {username}'
        if in_shell:
            full_command += f' /bin/bash -c "{command}"'
        else:
            full_command += f' {command}'
        self.log.debug(f"command: {full_command}")
        if capture:
            return subprocess.run(full_command, shell=True, capture_output=True, encoding='utf-8')
        else:
            return subprocess.run(full_command, shell=True)


    def set_perms(self,filename):
        if (os.path.exists(filename)):
            os.chmod(filename, 0o755)


    def copy_perms(self,template,final_folder):
        files = os.listdir(final_folder)
        for file in files:
            template_file = os.path.join(template,file)
            if not os.path.exists(template_file):
                continue
            if not os.path.isdir(template_file):
                continue
            final_file = os.path.join(final_folder,file)
            statinfo = os.stat(template_file,follow_symlinks=False)
            os.chmod(final_file, statinfo.st_mode)
            os.chown(final_file, statinfo.st_uid, statinfo.st_gid, follow_symlinks=False)
            if os.path.isdir(final_file):
                self.copy_perms(template_file, final_file)
