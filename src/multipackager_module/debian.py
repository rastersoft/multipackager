#!/usr/bin/env python3

# Copyright 2015 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of Multipackager
#
# Multipackager is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Multipackager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import os
import shutil
import multipackager_module.package_base
import time

class debian (multipackager_module.package_base.package_base):

    def __init__(self, configuration, distro_type, distro_name, architecture, cache_name = None):
        super().__init__(configuration, distro_type, distro_name, architecture, cache_name)
        self.env = {"PassEnvironment": "DEBIAN_FRONTEND NO_COLOR TERM", "DEBIAN_FRONTEND":"noninteractive", "NO_COLOR": "true", "TERM": "xterm-mono"}


    def update_installed_packages(self, path = None):
        self.log.debug(f"Updating installed packages {path}")
        self.installed_packages = []
        if path is None:
            path = self.working_path
        self.add_dns(path)
        completed = self.run_chroot_full(path, 'apt list --installed |cat', True)
        if completed.returncode != 0:
            self.log.warning("Failed to update packages")
            return
        for line in completed.stdout.split("\n"):
            line = line.strip()
            if line == "":
                continue
            pos1 = line.find("/")
            if pos1 == -1:
                continue
            self.installed_packages.append(line[:pos1])
        self.log.debug("Updated installed packages")


    def check_path_in_builds(self,project_path):
        if self.distro_type == "ubuntu":
            # Try the "ubuntu" folder, and if it doesn't exists, try with "debian" one
            if self.architecture == "i386":
                path_list = ["ubuntu32","UBUNTU32","Ubuntu32","ubuntu","UBUNTU","Ubuntu","debian32","DEBIAN32","Debian32","debian","DEBIAN","Debian"]
            else:
                path_list = ["ubuntu64","UBUNTU64","Ubuntu64","ubuntu","UBUNTU","Ubuntu","debian64","DEBIAN64","Debian64","debian","DEBIAN","Debian"]
        else:
            if self.architecture == "i386":
                path_list = ["debian32","DEBIAN32","Debian32","debian","DEBIAN","Debian"]
            else:
                path_list = ["debian64","DEBIAN64","Debian64","debian","DEBIAN","Debian"]

        for element in path_list:
            path = os.path.join(project_path, element)
            if os.path.exists(path):
                self.log.info(f"Check build path: {path}")
                return path
        return None


    def set_project_version(self, text):
        pos = text.rfind("-")
        if (pos != -1):
            text = text[:pos]
        self.project_version = text
        self.log.info(f"Set version to {text}")


    def get_package_name(self, project_path):
        """ Returns the final package name for the project specified, or None if can't be determined yet """

        is_python, pyfile = multipackager_module.package_base.package_base.check_is_python(project_path)

        architecture = self.architecture
        if (is_python):
            self.log.info(f"Reading python setup file {pyfile} at {project_path}")
            self.read_python_setup(project_path, pyfile)
            package_name = f"{"python2" if self.python2 else "python3"}-{self.project_name}-{self.distro_name}_{self.project_version}-{self.distro_type}{self.configuration.revision}_all.deb"
        else:
            debian_path = self.check_path_in_builds(project_path)
            if (debian_path == None):
                self.log.error("Debian path doesn't exist")
                return True

            control_path = os.path.join(debian_path,"control")
            if (not os.path.exists(control_path)):
                self.log.error("Control path doesn't exist")
                return True

            f = open (control_path,"r")
            for line in f:
                pos = line.find(':')
                if pos == -1:
                    continue
                command = line[:pos]
                data = line[pos+1:].strip()
                if command == "Source":
                    self.project_name = data
                elif command == "Package":
                    self.project_name = data
                elif command == "Version":
                    self.set_project_version(data)
                elif command == "Architecture" and data == "all":
                    architecture = "all"
            f.close()
            package_name = f"{self.project_name}-{self.distro_name}_{self.project_version}-{self.distro_type}{self.configuration.revision}_{architecture}.deb"
        self.log.info(f"Set package name to {package_name}")
        return [package_name]


    def generate(self,path):
        """ Ensures that the base system, to create a CHROOT environment, exists """

        # Create all, first, in a temporal folder
        tmp_path = path+".tmp"

        shutil.rmtree(tmp_path, ignore_errors=True)

        os.makedirs(tmp_path)
        if self.distro_type == "debian":
            server = "http://http.debian.net/debian/"
        else:
            server = "http://archive.ubuntu.com/ubuntu/"
        command = f"debootstrap --variant=buildd --arch {self.architecture} {self.distro_name} {tmp_path} {server}"

        if 0 != self.run_external_program(command):
            return True # error!!!

        f = open(os.path.join(tmp_path,"etc","apt","sources.list"),"w")
        if (self.distro_type == "debian"):
            # Add contrib and non-free to the list of packages sources if DEBIAN
            f.write(f"deb http://ftp.debian.org/debian/ {self.distro_name} main contrib non-free non-free-firmware\n")
        else:
            # Add restricted, universe and multiverse if UBUNTU
            f.write(f"deb http://archive.ubuntu.com/ubuntu/ {self.distro_name} main restricted universe multiverse\n")
        f.close()

        self.add_dns(tmp_path)

        command = 'apt clean'
        if (0 != self.run_chroot(tmp_path,command)):
            return True # error!!!

        command = 'apt install -f'
        if (0 != self.run_chroot(tmp_path,command)):
            return True # error!!!

        command = 'apt update'
        if (0 != self.run_chroot(tmp_path,command)):
            return True # error!!!

        time.sleep(5)
        command = 'apt install meson ninja-build -y'
        if (0 != self.run_chroot(tmp_path, command)):
            time.sleep(5)
            command = 'apt install -f'
            if (0 != self.run_chroot(tmp_path, command)):
                return True # error!!!

        os.sync()
        os.rename(tmp_path,path) # rename the folder to the definitive name
        os.sync()

        shutil.rmtree(tmp_path, ignore_errors=True)

        self.update_installed_packages(tmp_path)
        return False # no error


    def update(self,path):
        """ Ensures that the chroot environment is updated with the lastest packages """

        # Here, we have for sure the CHROOT environment, but maybe it must be updated

        self.add_dns(path)
        retval = self.run_chroot(path,"apt clean")
        if (retval != 0):
            return True # error!!!!

        retval = self.run_chroot(path,"apt update")
        if (retval != 0):
            return True # error!!!!

        retval = self.run_chroot(path,"apt full-upgrade -y")
        if (retval != 0):
            return True # error!!!!

        self.update_installed_packages()
        return False


    def install_dependencies_full(self, path, dependencies):
        self.update_installed_packages(path)
        retval = self.run_chroot(path, "apt clean")
        if (retval != 0):
            self.log.error("Failed to do apt clean")
            return True

        retval = self.run_chroot(path, "apt update")
        if (retval != 0):
            self.log.error("Failed to do apt update")
            return True

        command = "apt install -y"
        for dep in dependencies:
            if not isinstance(dep, str):
                continue
            if self.is_installed(dep):
                continue
            command += " "+dep
        retval = self.run_chroot(path, command)
        self.update_installed_packages(path)

        for dep in dependencies:
            if isinstance(dep, str):
                continue
            if self.is_installed(dep):
                continue
            installed = False
            for package in dep:
                retval = self.run_chroot(path, f"apt install -y {package}")
                if retval == 0:
                    installed = True
                    break
            if not installed:
                self.log.error(f"Can't install any of the packages in {dep}")
                return True
            self.update_installed_packages(path)
        return False


    def install_local_package_internal(self, file_name):
        self.log.info(f"Install local package {file_name}")
        retval = self.run_chroot(self.working_path,"apt clean")
        if (retval != 0):
            self.log.error(f"Failed to install {file_name} (clean)")
            return retval

        retval = self.run_chroot(self.working_path,"apt update")
        if (retval != 0):
            self.log.error(f"Failed to install {file_name} (update)")
            return True # error!!!!

        if 0 != self.run_chroot(self.working_path, f"dpkg -i {file_name}"):
            if 0 != self.run_chroot(self.working_path, "apt install -f -y"):
                self.log.error(f"Failed to install {file_name} (dpkg/apt)")
                return True
        self.update_installed_packages()
        return False


    def is_installed(self, package):
        self.log.debug(f"Checking if {package} is installed")
        if isinstance(package, str):
            return package in self.installed_packages
        for element in package:
            if element in self.installed_packages:
                return True
        return False


    def install_dependencies(self,project_path,avoid_packages,preinstall):
        """ Install the dependencies needed for building this package """

        self.log.info(f"Installing dependencies for {project_path}")
        dependencies = []
        debian_path = self.check_path_in_builds(project_path)

        if multipackager_module.package_base.package_base.check_is_python(project_path)[0]: # it is a python package
            control_path = os.path.join(debian_path,"control")
            if (not os.path.exists(control_path)):
                control_path = os.path.join(project_path,"stdeb.cfg")
            dependencies += ["python3", "python3-all", "fakeroot", "dh-python"]
        else:
            dependencies += ["meson", "ninja-build"]
            if debian_path == None:
                self.log.critical("There is no DEBIAN/UBUNTU folder with the package specific data")
                return True

            control_path = os.path.join(debian_path,"control")
            if (not os.path.exists(control_path)):
                self.log.critical("There is no CONTROL file with the package specific data")
                return True

        f = open (control_path,"r")
        last_command = None
        for line in f:
            is_dependency = False
            if line[0] == ' ':
                if last_command is None:
                    continue
                else:
                    if last_command.startswith("Depends") or last_command.startswith("Build-Depends"):
                        is_dependency = True
            else:
                if line.startswith("Depends") or line.startswith("Build-Depends"):
                    last_command = line
                    is_dependency = True
            if is_dependency:
                if line.startswith("Depends3"):
                    tmp = line[8:].strip()
                elif line.startswith("Depends"):
                    tmp = line[7:].strip()
                elif line.startswith("Build-Depends"):
                    tmp = line[13:].strip()
                else:
                    tmp = line.strip()
                if (tmp[0] == ':') or (tmp[0] == '='):
                    tmp = tmp[1:].strip()
                print(tmp)
                tmp = tmp.split(",")
                for element2 in tmp:
                    tmp2 = element2.split("|")
                    # if it is a single package, just add it as-is
                    if (len(tmp2) == 1):
                        pos = element2.find("(") # remove version info
                        if (pos != -1):
                            element2 = element2[:pos]
                        element2 = element2.strip()
                        if len(element2) == 0:
                            continue
                        if element2[0] == '$':
                            continue
                        dependencies.append(element2.strip())
                        continue
                    # but if there are several optional packages, add all of them as an array
                    list_p = []
                    for element in tmp2:
                        pos = element.find("(") # remove version info
                        if (pos != -1):
                            element = element[:pos]
                        list_p.append(element.strip())
                    dependencies.append(list_p)
                continue
            if line[:7] == "Source:":
                self.project_name = line[7:].strip()
                continue
            if line[:8] == "Package:":
                self.project_name = line[8:].strip()
                continue
            if line[:8] == "Version:":
                self.set_project_version(line[8:].strip())
                continue
            last_command = None
        f.close()

        if (len(dependencies) != 0):
            deps2 = []
            self.log.info(f"Installing {dependencies}")
            for d in dependencies:
                if avoid_packages.count(d) == 0:
                    deps2.append(d)
            return self.install_dependencies_full(self.base_path,deps2)
        return False


    def build_python(self):
        """ Builds a package for a python project """

        destination_dir = "/install_root"
        destination_dir_outside = os.path.join(self.working_path, "install_root")
        self.log.info(f"Deleting folder {destination_dir_outside}")
        shutil.rmtree(destination_dir_outside, ignore_errors = True)

        # remove any build artifacts
        shutil.rmtree(os.path.join(self.working_path, "build"), ignore_errors=True)
        shutil.rmtree(os.path.join(self.working_path, ".build"), ignore_errors=True)
        shutil.rmtree(os.path.join(self.working_path, "_build"), ignore_errors=True)
        self.log.info(f"Building python binaries at {destination_dir}, in working folder {self.working_path}")
        if (self.run_chroot(self.working_path, f'cd /project && pybuild --disable test --dest-dir {destination_dir}')):
            self.log.info("Failed to run pybuild")
            return True
        return False


    def copy_pacs(self, destination_dir, package_name):
        self.log.info(f"Copying pac {package_name} to {destination_dir}")
        files = os.listdir(destination_dir)
        for f in files:
            if f[-4:] == ".deb":
                origin_name = os.path.join(destination_dir,f)
                final_name = os.path.join(os.getcwd(),package_name)
                if (os.path.exists(final_name)):
                    os.remove(final_name)
                if os.path.isdir(origin_name):
                    if not self.copy_pacs(origin_name,package_name):
                        return False
                shutil.move(origin_name, final_name)
                return False
        return True


    def build_package(self, project_path):
        """ Takes the binaries located at /install_root and build a package """

        self.log.info(f"build package at {project_path}")
        debian_path = self.check_path_in_builds(project_path)

        package_path = os.path.join(self.working_path,"install_root","DEBIAN")
        os.makedirs(package_path)
        command = f"cp -a {os.path.join(debian_path,"*")} {package_path}"
        if 0 != self.run_external_program(command):
            return True

        self.set_perms(os.path.join(package_path,"preinst"))
        self.set_perms(os.path.join(package_path,"postinst"))
        self.set_perms(os.path.join(package_path,"prerm"))
        self.set_perms(os.path.join(package_path,"postrm"))

        control_path = os.path.join(package_path,"control")
        f1 = open (control_path,"r")
        f2 = open (control_path+".tmp","w")
        last_command = None
        for line in f1:
            line = line.replace("\n","").replace("\r","")
            if (line == ""): # remove blank lines, just in case
                continue
            elif (line[:13] == "Architecture:"):
                arch = line[13:].strip()
                if (arch == "any"):
                    line = f"Architecture: {self.architecture}"
            elif (line[:7] == "Source:"):
                continue
            elif (line[:14] == "Build-Depends:"):
                last_command = line
                continue
            if (line[0] == ' ' and last_command is not None and last_command.startswith("Build-Depends:")):
                continue
            elif (line[:8] == "Version:"):
                continue
            elif (line[:15] == "Installed-Size:"):
                continue
            last_command = None
            f2.write(line+"\n")
        f2.write(f"Version: {self.project_version}\n")
        f2.write(f"Installed-Size: {int(self.program_size/1024)}\n")
        f1.close()
        f2.close()
        os.remove(control_path)
        os.rename(control_path+".tmp",control_path)
        package_name = self.get_package_name(self.build_path)[0]
        command = f'cd / && dpkg -b /install_root {package_name}'
        if (self.run_chroot(self.working_path, command)):
            return True
        shutil.move(os.path.join(self.working_path,package_name), os.getcwd())
        return False
